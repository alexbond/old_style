class OldStyleException(Exception):
    pass


class StateNameError(OldStyleException):
    pass


class PriceIncorrectValue(OldStyleException):
    pass
