from dataclasses import dataclass
from typing import Union


@dataclass
class Price:
    value: Union[int, float]

    def __post_init__(self):
        self.value = float(self.value)
        if self.value < 0.0:
            raise ValueError(f"{self.__class__.__name__} has to be positive, current is {self.value}")
