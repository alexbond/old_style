import abc

from ca_improvements.pricer.domain.price import Price
from ca_improvements.pricer.domain.tax_area import TaxArea


class TotalPrice(abc.ABC):

    @abc.abstractmethod
    def value(self, vendor_price: Price) -> Price:
        pass


class TotalPriceViaTaxArea(TotalPrice):

    def __init__(self, tax_area: TaxArea):
        self.tax_area = tax_area

    def value(self, vendor_price: Price) -> Price:
        total_price = Price(value=(vendor_price.value * (1 + self.tax_area.tax)))
        return total_price
