import abc

from ca_improvements.pricer.domain.state import StateName


class StatesTaxRegistry(abc.ABC):
    @abc.abstractmethod
    def tax_for(self, state: StateName) -> float:
        pass
