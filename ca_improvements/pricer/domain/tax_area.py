import abc
from dataclasses import dataclass

from ca_improvements.pricer.domain.registry import StatesTaxRegistry
from ca_improvements.pricer.domain.state import StateName


class TaxArea(abc.ABC):
    @abc.abstractmethod
    @property
    def tax(self) -> float:
        pass


@dataclass
class State(TaxArea):
    state: StateName
    tax_registry: StatesTaxRegistry

    @property
    def tax(self) -> float:
        return self.tax_registry.tax_for(self.state)


@dataclass
class Europe(TaxArea):
    value: float = 0.18

    @property
    def tax(self) -> float:
        return self.value
