from dataclasses import dataclass

from ca_improvements.pricer.domain.exceptions import StateNameError


@dataclass
class StateName:
    name: str

    def __post_init__(self):
        if self.name not in ["TX", "DE"]:
            raise StateNameError(f"{self.name} is unknown state.")