from dataclasses import dataclass


@dataclass
class Tax:
    value: float

    def __post_init__(self):
        if self.value < 0.0 or self.value > 1.0:
            raise ValueError(f"{self.__class__.__name__} has to be between 0 and 1. Current value is: {self.value}.")
