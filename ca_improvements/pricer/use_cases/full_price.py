import logging
from dataclasses import dataclass

from ca_improvements.pricer.domain.exceptions import PriceIncorrectValue
from ca_improvements.pricer.domain.full_price import TotalPriceViaTaxArea
from ca_improvements.pricer.domain.price import Price


@dataclass
class PriceForState:
    total_price: TotalPriceViaTaxArea

    def value(self, vendor_price: float) -> float:
        try:
            price = Price(vendor_price)
            total_price = self.total_price.value(vendor_price=price).value
            return total_price
        except PriceIncorrectValue as exc:
            logging.exception(exc)
            raise
