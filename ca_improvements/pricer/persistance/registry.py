from dataclasses import dataclass

from ca_improvements.pricer.domain.registry import StatesTaxRegistry
from ca_improvements.pricer.domain.state import StateName
from ca_improvements.pricer.persistance.dal_client import DALClient


@dataclass
class StatesTaxRegistryService(StatesTaxRegistry):
    dao: DALClient

    def tax_for(self, state: StateName) -> float:
        tax = self.dao.get_taxes(state.name)
        return tax
