import abc
from dataclasses import dataclass

from ca_improvements.api.dal import get_taxes


@dataclass
class DALClient:
    @abc.abstractmethod
    def get_taxes(self, state: str) -> float:
        pass


class DALConfig:
    pass


class EmptyConfig(DALConfig):
    pass


@dataclass
class DALClientJSON(DALClient):
    config: DALConfig

    def get_taxes(self, state: str) -> float:
        return get_taxes(state)
