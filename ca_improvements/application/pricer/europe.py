from ca_improvements.pricer.domain.full_price import TotalPriceViaTaxArea
from ca_improvements.pricer.domain.tax_area import Europe
from ca_improvements.pricer.use_cases.full_price import PriceForState


def price_for_europe(vendor_price: float) -> float:
    tax_area = Europe()
    total_price = TotalPriceViaTaxArea(tax_area=tax_area)
    price = PriceForState(total_price=total_price)
    return price.value(vendor_price=vendor_price)
