from ca_improvements.pricer.domain.full_price import TotalPriceViaTaxArea
from ca_improvements.pricer.domain.state import StateName
from ca_improvements.pricer.domain.tax_area import State
from ca_improvements.pricer.persistance.dal_client import DALClientJSON, EmptyConfig
from ca_improvements.pricer.persistance.registry import StatesTaxRegistryService
from ca_improvements.pricer.use_cases.full_price import PriceForState


def _price_for_state(vendor_price: float, state: str):
    config = EmptyConfig()
    dao = DALClientJSON(config=config)
    tax_registry = StatesTaxRegistryService(dao=dao)
    state_name = StateName(state)
    tax_area = State(state=state_name, tax_registry=tax_registry)
    total_price = TotalPriceViaTaxArea(tax_area=tax_area)
    price = PriceForState(total_price=total_price)
    return price.value(vendor_price=vendor_price)


def price_for_texas(vendor_price: float) -> float:
    return _price_for_state(vendor_price, "TX")


def price_for_delaware(vendor_price: float) -> float:
    return _price_for_state(vendor_price, "DE")
