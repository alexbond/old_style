class OldStyleException(Exception):
    pass


class PriceException(OldStyleException):
    pass
