import logging
from typing import Final, Optional

from old_style_small_improvements.dal import tax_for
from old_style_small_improvements.exceptions import PriceException


EU_TAX: Final[float] = 0.18


# price: Union[float, int] -> price: float
# _calculate_full_price -> _full_price
# introduce total_price
# add specific exception
def _full_price(price: float, tax: float) -> float:
    if price < 0.0:
        raise PriceException("price cannot be negative.")
    total_price = price * (1.0 + tax)
    return total_price


# _get_total_price_for_state -> price_for_state
# price: float -> vendor_price: float
# state: str = "CA" -> state: str
# usa: bool = True -> europe_area_tax: Optional[float]
def _price_for_state(vendor_price: float, state: str, europe_area_tax: Optional[float]):
    if europe_area_tax is None:
        tax = tax_for(state)
    else:
        tax = europe_area_tax

    try:
        full_price = _full_price(vendor_price, tax)
    except PriceException as ex:
        logging.exception(ex)
        full_price = None

    return full_price


# price: Union[float, int] -> vendor_price: float
# avoiding duplication of exception handling
def price_for_delaware(vendor_price: float) -> Optional[float]:
    return _price_for_state(vendor_price, "DE", europe_area_tax=None)


# price: Union[float, int] -> vendor_price: float
def price_for_texas(vendor_price: float) -> Optional[float]:
    return _price_for_state(vendor_price, "TX", europe_area_tax=None)


# price: Union[float, int] -> vendor_price: float
# introduce fixed europe tax as constant
def total_price_for_europe(vendor_price: float) -> Optional[float]:
    return _price_for_state(vendor_price, "", europe_area_tax=EU_TAX)
