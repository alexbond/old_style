from typing import Optional, Union

from old_style.dal import get_taxes


def _calculate_full_price(price: Union[float, int], tax: float) -> float:
    if price < 0:
        raise ValueError("price cannot be negative.")
    if isinstance(price, float):
        return price * (1.0 + tax)
    if isinstance(price, int):
        return float(price) * (1.0 + tax)  # hidden ValueError e.g. ValueError: could not convert string to float

    # hidden return None e.g. price is positive Decimal


def _get_total_price_for_state(price: Union[float, int], state: str = "CA", usa: bool = True) -> float:
    if usa:
        tax = get_taxes(state)
    else:
        # Comment: Because in EU tax is fixed
        tax = 0.18
    return _calculate_full_price(price, tax)


def total_price_for_delaware(price: Union[float, int]) -> Optional[float]:
    try:
        return _get_total_price_for_state(price, "DE")
    except ValueError:
        return None


def total_price_for_texas(price: Union[float, int]) -> Optional[float]:
    try:
        return _get_total_price_for_state(price, "TX")
    except:
        return None


def total_price_for_europe(price: Union[float, int]) -> Optional[float]:
    try:
        return _get_total_price_for_state(price, "", False)
    except:
        return None
