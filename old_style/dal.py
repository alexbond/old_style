def get_taxes(state: str) -> float:
    # post request, process json, extract value....
    tax = ...

    if state == "DE":
        return 0.05 * tax
    if state == "TX":
        return 0.15 * tax
